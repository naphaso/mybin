#!/bin/bash

VIDEOPATH=/media/data/video/my

control_c() {
  ffmpeg -f image2 -i img/%d.jpg video.mpg
  popd
  exit 0
}
trap control_c SIGINT

VIDEODIR=$VIDEOPATH/`date +%d.%m.%Y`/$1
mkdir -p $VIDEODIR/img
pushd $VIDEODIR

i=1
while true; do
	scrot -d 1 "img/$i.jpg"
	i=$((i+1))
done


